
class ResultadoI extends Calculadora.Resultado {

    getResultado(resultado) {
        document.getElementById('resultado').innerHTML = resultado;
    }
}

async function operar(tipoOperacion) {
    let communicator;
    try {
        communicator = Ice.initialize();

        
        //Crea la conexión con el servidor para enviar la información
        const proxy = await Calculadora.OperacionesPrx.checkedCast(communicator.stringToProxy("Calculadora:tcp -p 10000:ws -h localhost -p 9000"));
        
        /*
         * Crea un adaptador, sin nombre ni endpoints para recibir callbacks de conexiones
         * bidireccionales
         */
        const adapter = await communicator.createObjectAdapter("");

        
        //Registra el callback de recepción con el objeto adaptador y lo activa
        const receiver = Calculadora.ResultadoPrx.uncheckedCast(adapter.addWithUUID(new ResultadoI()));
        
        //Asocia el objeto adaptador con la conexión bidireccional
        proxy.ice_getCachedConnection().setAdapter(adapter);

        //Se obtienen los números para la operación aritmética
        const a = parseInt(document.getElementById('primerNumero').value);
        const b = parseInt(document.getElementById('segundoNumero').value);

        // Provee el proxy para recepcionar el callback desde el servidor y espera para
        // terminar la conexión
        await proxy[tipoOperacion](receiver, a, b);
        await communicator.waitForShutdown();

    } catch(ex) {
        console.log(ex.toString());
        if(communicator) {
            await communicator.destroy();
        }
    }
};
