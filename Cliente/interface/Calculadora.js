//
// Copyright (c) ZeroC, Inc. All rights reserved.
//
//
// Ice version 3.7.3
//
// <auto-generated>
//
// Generated from file `Calculadora.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

/* eslint-disable */
/* jshint ignore: start */

(function(module, require, exports)
{
    const Ice = require("ice").Ice;
    const _ModuleRegistry = Ice._ModuleRegistry;
    const Slice = Ice.Slice;

    let Calculadora = _ModuleRegistry.module("Calculadora");

    const iceC_Calculadora_Resultado_ids = [
        "::Calculadora::Resultado",
        "::Ice::Object"
    ];

    Calculadora.Resultado = class extends Ice.Object
    {
    };

    Calculadora.ResultadoPrx = class extends Ice.ObjectPrx
    {
    };

    Slice.defineOperations(Calculadora.Resultado, Calculadora.ResultadoPrx, iceC_Calculadora_Resultado_ids, 0,
    {
        "getResultado": [, , , , [7], [[7]], , , , ]
    });

    const iceC_Calculadora_Operaciones_ids = [
        "::Calculadora::Operaciones",
        "::Ice::Object"
    ];

    Calculadora.Operaciones = class extends Ice.Object
    {
    };

    Calculadora.OperacionesPrx = class extends Ice.ObjectPrx
    {
    };

    Slice.defineOperations(Calculadora.Operaciones, Calculadora.OperacionesPrx, iceC_Calculadora_Operaciones_ids, 0,
    {
        "sumar": [, , , , , [["Calculadora.ResultadoPrx"], [3], [3]], , , , ],
        "restar": [, , , , , [["Calculadora.ResultadoPrx"], [3], [3]], , , , ],
        "dividir": [, , , , , [["Calculadora.ResultadoPrx"], [3], [3]], , , , ],
        "multiplicar": [, , , , , [["Calculadora.ResultadoPrx"], [3], [3]], , , , ]
    });
    exports.Calculadora = Calculadora;
}
(typeof(global) !== "undefined" && typeof(global.process) !== "undefined" ? module : undefined,
 typeof(global) !== "undefined" && typeof(global.process) !== "undefined" ? require :
 (typeof WorkerGlobalScope !== "undefined" && self instanceof WorkerGlobalScope) ? self.Ice._require : window.Ice._require,
 typeof(global) !== "undefined" && typeof(global.process) !== "undefined" ? exports :
 (typeof WorkerGlobalScope !== "undefined" && self instanceof WorkerGlobalScope) ? self : window));
