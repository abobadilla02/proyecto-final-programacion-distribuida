const communicator = Ice.initialize();
const proxy = communicator.stringToProxy("Calculadora:ws -h localhost -p 9000");

async function sumar() {
    try {
        const test = await Calculadora.OperacionesPrx.checkedCast(proxy);
        await test.printUp();
    }
    catch (ex) {
        console.log(ex.toString());
    }
    finally {
        // if (communicator) {
        //     return communicator.destroy();
        // }
    }
}

async function restar() {
    try {
        const test = await Calculadora.OperacionesPrx.checkedCast(proxy);
        await test.printDown();
    }
    catch (ex) {
        console.log(ex.toString());
    }
    finally {
        // if (communicator) {
        //     return communicator.destroy();
        // }
    }
}

async function multiplicar() {
    try {
        const test = await Calculadora.OperacionesPrx.checkedCast(proxy);
        await test.printLeft();
    }
    catch (ex) {
        console.log(ex.toString());
    }
    finally {
        // if (communicator) {
        //     return communicator.destroy();
        // }
    }
}

async function dividir() {
    try {
        const test = await Calculadora.OperacionesPrx.checkedCast(proxy);
        console.log(test);
        await test.printRight();
    }
    catch (ex) {
        console.log(ex.toString());
    }
    finally {
        // if (communicator) {
        //     return communicator.destroy();
        // }
    }
}