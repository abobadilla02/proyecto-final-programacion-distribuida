module Calculadora {

    interface Resultado {
    	string getResultado(string resultado);
    }

    interface Operaciones {
        void sumar(Resultado* result, int a, int b);
        void restar(Resultado* result, int a, int b);
        void dividir(Resultado* result, int a, int b);
        void multiplicar(Resultado* result, int a, int b); 
    }

}