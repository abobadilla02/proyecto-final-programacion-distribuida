
public class Server {

    public static void main(String[] args) {
        
        /**
         * Se agregan parámetros adicionales debido a que se usa una conexión
         * bidireccional. Es decir:
         * 1) El servidor está escuchando
         * 2) El cliente genera la acción (operación aritmética)
         * 3) El servidor lo recibe, lo procesa, obtiene el resultado
         * 4) Y como es bidireccional, el servidor le envía el resultado 
         *    nuevamente al cliente
         * 5) El cliente despliega el resultado en pantalla
         */
        
        int status = 0;
        java.util.List<String> extraArgs = new java.util.ArrayList<String>();

        try (com.zeroc.Ice.Communicator communicator = com.zeroc.Ice.Util.initialize(args, "config.server", extraArgs)) {
            communicator.getProperties().setProperty("Ice.Default.Package", "com.zeroc.demos.Ice.bidir");
            Runtime.getRuntime().addShutdownHook(new Thread(() -> communicator.destroy()));

            if (!extraArgs.isEmpty()) {
                System.err.println("Muchos argumentos");
                status = 1;
            } else {

                com.zeroc.Ice.ObjectAdapter adapter = communicator.createObjectAdapter("Callback.Server");
                final OperacionesI sender = new OperacionesI();
                Runtime.getRuntime().addShutdownHook(new Thread(() -> sender.destroy()));
                adapter.add(sender, com.zeroc.Ice.Util.stringToIdentity("Calculadora"));
                adapter.activate();
                sender.start();
                communicator.waitForShutdown();
            }
        }

        System.exit(status);
    }
}
