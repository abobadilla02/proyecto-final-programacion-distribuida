
import com.zeroc.Ice.Current;
import Calculadora.ResultadoPrx;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class OperacionesI implements Calculadora.Operaciones {
    
    /**
     * Las operaciones sumar, restar, dividir y multiplicar, quedaron finalmente
     * en void, ya que para lograr hacer el callback de vuelta a Javascript,
     * es necesario agregar los valores a un ArrayList global, por lo que no
     * se justifica mantener los retornos
     * 
     * Se calcula, se convierte a string y se añade en un ArrayList el
     * identificador del resultado y del callback en si. Y por otro lado
     * se guarda el resultado.
     */
    
    
    @Override
    synchronized public void sumar(ResultadoPrx result, int a, int b, Current current) {
        int suma = a + b;
        System.out.println("Agregar resultado suma `" + com.zeroc.Ice.Util.identityToString(result.ice_getIdentity()) + "'");
        _resultados.add(result.ice_fixed(current.con));
        _valores.add(Integer.toString(suma));
    }

    @Override
    synchronized public void restar(ResultadoPrx result, int a, int b, Current current) {
        int resta = a - b;
        System.out.println("Agregar resultado resta `" + com.zeroc.Ice.Util.identityToString(result.ice_getIdentity()) + "'");
        _resultados.add(result.ice_fixed(current.con));
        _valores.add(Integer.toString(resta));
    }

    @Override
    synchronized public void dividir(ResultadoPrx result, int a, int b, Current current) {
        float division = a / b;
        System.out.println("Agregar resultado division `" + com.zeroc.Ice.Util.identityToString(result.ice_getIdentity()) + "'");
        _resultados.add(result.ice_fixed(current.con));
        _valores.add(String.valueOf(division));
    }

    @Override
    synchronized public void multiplicar(ResultadoPrx result, int a, int b, Current current) {
        int multiplicacion = a * b;
        
        System.out.println("Agregar resultado multiplicacion `" + com.zeroc.Ice.Util.identityToString(result.ice_getIdentity()) + "'");
        _resultados.add(result.ice_fixed(current.con));
        _valores.add(Integer.toString(multiplicacion));
    }

    public void destroy() {
        System.out.println("Se destruye el callback que se envía a javascript");
        _executorService.shutdown();
        try {
            _executorService.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException interrupted) {
            // ignored
        }
    }
    
    /**
     * Se inicia el hilo y se invoca el callback cada 2 segundos 
     */
    public void start() {
        _executorService.scheduleAtFixedRate(() -> invokeCallback(), 2, 2, TimeUnit.SECONDS);
    }
    
    /**
     * Constantemente verifica que hayan resultados nuevos, para retornarlo
     * al cliente en javascript (aplicación web)
     */
    synchronized private void invokeCallback() {
        if (!_resultados.isEmpty()) {

            /**
             * Se recorre el arreglo de resultados y se invoca la función 
             * getResultado para enviar el valor obtenido
             */
            for (ResultadoPrx p : _resultados) {
                try {
                    String valor = _valores.get(_resultados.indexOf(p));
                    
                    p.getResultadoAsync(valor).whenCompleteAsync(
                            (r, t)
                            -> {
                        if (t != null) {
                            removeResultado(p, t, valor);
                        }
                    }, _executorService);
                } catch (com.zeroc.Ice.CommunicatorDestroyedException e) {
                    // Termina
                    break;
                }
            }
        }
    }
    
    /**
     * Se elimina el resultado y el valor de sus ArrayList
     * @param p
     * @param t
     * @param valor 
     */
    synchronized private void removeResultado(ResultadoPrx p, Throwable t, String v) {
        if (_resultados.remove(p)) {
            _valores.remove(v);
            System.err.println("removing client `" + com.zeroc.Ice.Util.identityToString(p.ice_getIdentity()) + "':");
            t.printStackTrace();
        }
    }

    private ScheduledExecutorService _executorService = Executors.newSingleThreadScheduledExecutor();
    private java.util.List<ResultadoPrx> _resultados = new java.util.ArrayList<>();
    private ArrayList<String> _valores = new ArrayList<String>();
}
