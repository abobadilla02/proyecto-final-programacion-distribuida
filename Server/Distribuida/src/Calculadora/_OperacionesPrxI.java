//
// Copyright (c) ZeroC, Inc. All rights reserved.
//
//
// Ice version 3.7.3
//
// <auto-generated>
//
// Generated from file `Calculadora.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package Calculadora;

/** @hidden */
public class _OperacionesPrxI extends com.zeroc.Ice._ObjectPrxI implements OperacionesPrx
{
    /** @hidden */
    public static final long serialVersionUID = 0L;
}
